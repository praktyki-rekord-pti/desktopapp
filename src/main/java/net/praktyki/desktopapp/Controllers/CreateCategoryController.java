package net.praktyki.desktopapp.Controllers;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

public class CreateCategoryController {
    @FXML
    public TextField name;

    @FXML
    public TextArea description;

    @FXML
    public ColorPicker colorPicker;

    @FXML
    public Button create;

    @FXML
    public FontAwesomeIconView leave;

    @FXML
    public void onButtonCreateClick(ActionEvent e){

    }

    @FXML
    public void onButtonLeaveClick(MouseEvent e){

    }
}
