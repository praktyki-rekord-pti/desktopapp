package net.praktyki.desktopapp.Controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import net.praktyki.desktopapp.App;
import net.praktyki.desktopapp.Objects.Category;
import net.praktyki.desktopapp.Objects.Requests.AddCategoryRequest;
import net.praktyki.desktopapp.Objects.Requests.AddTaskRequest;
import net.praktyki.desktopapp.Objects.Requests.LoginRequest;
import net.praktyki.desktopapp.Objects.Responses.DefaultResponse;
import net.praktyki.desktopapp.Objects.Task;
import net.praktyki.desktopapp.Site;
import net.praktyki.desktopapp.Utils;
import java.time.LocalDateTime;

import static net.praktyki.desktopapp.Utils.jackson;

public class LoginController {
    @FXML
    public TextField emailField;

    @FXML
    public PasswordField passwordField;

    @FXML
    public void onLoginClick(ActionEvent e){
        var app = App.app;
        var email = emailField.getText();
        var password = passwordField.getText();
        var endpoint = "/login";
        var json = new Gson().toJson(new LoginRequest(email,password));
        Platform.runLater(new Runnable() {
            @Override
            public void run(){
                try{
                    var response = Utils.callPost(endpoint,json);
                    var responseObject = new Gson().fromJson(response, DefaultResponse.class);
                    if(responseObject.response) app.loadStage(Site.CATEGORIES);
                    else{
                        var alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Logowanie");
                        alert.setHeaderText("Odpowiedź od serwera:");
                        alert.setContentText("Podane dane logowania nie są poprawne.");

                        alert.showAndWait();
                    }
                }catch (Exception ex){
                    var alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Logowanie");
                    alert.setHeaderText("Nie można połączyć się z serwerem.");
                    alert.setContentText("Sprawdź swoje połączenie i spróbuj ponownie.");

                    alert.showAndWait();
                }
            }
        });
    }

    //    var email = "micro@soft.com";
//        var password = "WindowsIsFarSuperior123";
}
