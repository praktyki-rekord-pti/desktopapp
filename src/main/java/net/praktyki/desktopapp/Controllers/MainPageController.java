package net.praktyki.desktopapp.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

import java.net.URL;
import java.util.ResourceBundle;

public class MainPageController implements Initializable {

    @FXML public ListView listView;

    public MainPageController() { }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {

        listView.getItems().addAll("Dom", "Praca", "Szkoła", "Studia");
    }
}
