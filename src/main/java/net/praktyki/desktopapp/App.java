package net.praktyki.desktopapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Objects;

/**
 * JavaFX App
 */
public class App extends Application {

    public static App app;

    public Stage stage;

    public void loadStage(Site site){
        switch (site){
            case TASKS -> loadStage("tasks");
            case CATEGORIES -> loadStage("categories");
            case CREATE_CATEGORY -> loadStage("createCategory");
            case CREATE_TASK -> loadStage("createTask");
            case MAIN_PAGE -> loadStage("mainPage");
            default -> loadStage("login");
        }
    }

    private void loadStage(String name){
        URL fxml;
        try{
            fxml = getClass().getClassLoader().getResource(String.format("%s.fxml",name));
            assert fxml != null;
            Parent root = FXMLLoader.load(fxml);
            var scene = new Scene(root, 640, 480);
            stage.setScene(scene);
            stage.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void start(Stage _stage) {
        stage = _stage;
        loadStage(Site.MAIN_PAGE);
        app = this;
    }


    public static void main(String[] args) {
        launch();
    }
}
