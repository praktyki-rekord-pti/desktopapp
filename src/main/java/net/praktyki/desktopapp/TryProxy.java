package net.praktyki.desktopapp;

import java.lang.reflect.Constructor;

public class TryProxy {

    public boolean handle = true;
    public boolean print = false;

    public Object proxyObject(){return new Object();}
    public void proxyVoid(){};

    public Object get() {
        Object output = null;
        try{
            output = proxyObject();
        }catch (Exception ex){
            if(!handle){
                throw ex;
            }
            if(print){
                ex.printStackTrace();
            }
        }
        return output;
    }

    public Object get(Object defaultOutput) {
        Object output = defaultOutput;
        try{
            output = proxyObject();
        }catch (Exception ex){
            if(!handle){
                throw ex;
            }
            if(print){
                ex.printStackTrace();
            }
        }
        return output;
    }

    public void run(){
        try{
            proxyVoid();
        }catch (Exception ex){
            if(!handle){
                throw ex;
            }
            if(print){
                ex.printStackTrace();
            }
        }
    };
}
