package net.praktyki.desktopapp.Objects;

public class Task {
    public long id;
    public long category_id;
    public long user_id;
    public String deadline;
    public short state;
    public String name;
    public String color;
    public int position;
    public String description;

    public Task(long id, long category_id, long user_id, String deadline, short state, String name, String color, int position, String description){
        this.id = id;
        this.category_id = category_id;
        this.user_id = user_id;
        this.deadline = deadline;
        this.state = state;
        this.name = name;
        this.color = color;
        this.position = position;
        this.description = description;
    }
}
