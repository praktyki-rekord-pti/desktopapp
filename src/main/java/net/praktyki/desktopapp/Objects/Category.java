package net.praktyki.desktopapp.Objects;

public class Category {
    public long id;
    public long user_id;
    public String name;
    public String color;
    public int position;
    public String description;

    public Category(long id, long user_id, String name, String color, int position, String description){
        this.id = id;
        this.user_id = user_id;
        this.name = name;
        this.color = color;
        this.position = position;
        this.description = description;
    }
}
