package net.praktyki.desktopapp.Objects.Responses;

import com.google.gson.annotations.SerializedName;

public class DefaultResponse {
    @SerializedName(value="response",alternate = {"Response","RESPONSE"})
    public boolean response;

    public DefaultResponse(boolean response){
        this.response = response;
    }
}
