package net.praktyki.desktopapp.Objects.Requests;

import net.praktyki.desktopapp.Objects.Task;

public class AddTaskRequest {
    public String email;
    public String password;
    public Task task;

    public AddTaskRequest(String email, String password, Task task){
        this.email = email;
        this.password = password;
        this.task = task;
    }
}
