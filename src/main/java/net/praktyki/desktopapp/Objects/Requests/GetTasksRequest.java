package net.praktyki.desktopapp.Objects.Requests;

public class GetTasksRequest {
    public String email;
    public String password;
    public long category_id;

    public GetTasksRequest(String email, String password, long category_id){
        this.email = email;
        this.password = password;
        this.category_id = category_id;
    }
}
