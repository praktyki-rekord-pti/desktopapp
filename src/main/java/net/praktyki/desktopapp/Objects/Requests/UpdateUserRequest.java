package net.praktyki.desktopapp.Objects.Requests;

import net.praktyki.desktopapp.Objects.User;

public class UpdateUserRequest {
    public String email;
    public String password;
    public User user;

    public UpdateUserRequest(String email, String password, User user){
        this.email = email;
        this.password = password;
        this.user = user;
    }
}
