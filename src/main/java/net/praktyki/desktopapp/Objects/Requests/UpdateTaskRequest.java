package net.praktyki.desktopapp.Objects.Requests;

import net.praktyki.desktopapp.Objects.Task;

import java.util.ArrayList;

public class UpdateTaskRequest {
    public String email;
    public String password;
    public ArrayList<Task> tasks = new ArrayList<>();

    public UpdateTaskRequest(String email, String password, ArrayList<Task> tasks){
        this.email = email;
        this.password = password;
        this.tasks.addAll(tasks);
    }
}
