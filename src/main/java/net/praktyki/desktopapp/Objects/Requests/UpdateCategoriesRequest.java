package net.praktyki.desktopapp.Objects.Requests;

import net.praktyki.desktopapp.Objects.Category;

import java.util.ArrayList;

public class UpdateCategoriesRequest {
    public String email;
    public String password;
    public ArrayList<Category> categories = new ArrayList<>();

    public UpdateCategoriesRequest(String email, String password, ArrayList<Category> categories){
        this.email = email;
        this.password = password;
        this.categories.addAll(categories);
    }
}
