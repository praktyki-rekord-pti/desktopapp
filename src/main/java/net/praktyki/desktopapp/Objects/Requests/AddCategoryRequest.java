package net.praktyki.desktopapp.Objects.Requests;

import net.praktyki.desktopapp.Objects.Category;

public class AddCategoryRequest {
    public String email;
    public String password;
    public Category category;

    public AddCategoryRequest(String email, String password, Category category){
        this.email = email;
        this.password = password;
        this.category = category;
    }
}
