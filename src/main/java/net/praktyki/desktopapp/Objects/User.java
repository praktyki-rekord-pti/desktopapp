package net.praktyki.desktopapp.Objects;

public class User {
    public long id;
    public String name;
    public String email;
    public String password;

    public User(long id, String name, String email, String password){
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
