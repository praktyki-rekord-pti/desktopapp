package net.praktyki.desktopapp;

public enum Site {
    LOGIN,
    CATEGORIES,
    TASKS,
    CREATE_CATEGORY,
    CREATE_TASK,
    MAIN_PAGE,
}
