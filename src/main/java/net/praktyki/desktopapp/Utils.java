package net.praktyki.desktopapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import okhttp3.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {
    private static String url = "http://alastor.overmc.net:34000/api";
    private static OkHttpClient client = new OkHttpClient();

    public static ObjectMapper jackson(){
        return JsonMapper.builder().addModule(new ParameterNamesModule())
                .addModule(new Jdk8Module())
                .addModule(new JavaTimeModule())
                .build();
    }

    public static String callPost(String endpoint, String json){
        var output = "";
        var body = RequestBody.create(
          MediaType.parse("application/json"), json
        );
        var request = new Request.Builder()
                .url(String.format("%s%s",url,endpoint))
                .post(body)
                .build();
        var call = client.newCall(request);
        try{
            var response = call.execute();
            output = response.body().string();
        }catch (Exception ex){
            output = null;
        }
        return output;
    }

    public static String localDateTimeToDateTime(LocalDateTime ldt){
        var output = new StringBuilder();
        output.append(ldt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        output.append("T");
        output.append(ldt.format(DateTimeFormatter.ofPattern("HH:mm:ss.SSSSSSS")));
        output.append("+02:00");
        return new String(output);
    }

    public static String replaceDateTimePlaceHolder(String json, LocalDateTime ldt){
        return json.replace("{Place$Holder}",localDateTimeToDateTime(ldt));
    }
}
