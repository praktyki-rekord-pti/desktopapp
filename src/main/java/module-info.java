module net.praktyki.desktopapp {
    requires javafx.controls;
    requires javafx.base;
    requires com.google.gson;
    requires org.apache.commons.codec;
    requires javafx.fxml;
    requires okhttp3;
    requires java.base;

    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires com.fasterxml.jackson.datatype.jdk8;
    requires com.fasterxml.jackson.module.paramnames;
    requires kotlin.stdlib;

    requires javafx.graphics;

    requires de.jensd.fx.glyphs.commons;
    requires de.jensd.fx.glyphs.fontawesome;

    exports net.praktyki.desktopapp;
    exports net.praktyki.desktopapp.Controllers;
    exports net.praktyki.desktopapp.Objects;
    exports net.praktyki.desktopapp.Objects.Requests;
    exports net.praktyki.desktopapp.Objects.Responses;
}